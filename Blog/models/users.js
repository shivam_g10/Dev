/**
 * Created by Shivam Mathur on 07-08-2016.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var userSchema = new Schema({
    username: String,
    password: String,
    email: String,
    name: String,
    article:[],
    admin: Boolean,
    DoB:{
        type:Date,
        default: Date.now()
    }
});
mongoose.model('users',userSchema);

