/**
 * Created by Shivam Mathur on 03-12-2016.
 */
var path = require('path');
var crypto = require('crypto');
var express = require('express');
var router = express.Router();

var Hexo = require('hexo');
var h = new Hexo(process.cwd(), {
    debug:true
});
h.init().then(function(param){
    if(param){
        console.log(param);
    }else{
        console.log('no param');
    }
    console.log('init');
});
h.watch().then(function(param){
    if(param){
        console.log(param);
    }else{
        console.log('no param');
    }
    console.log('Load');
});
h.call('generate',{}).then(function(){
    return h.exit();
}).catch(function(err){
    return h.exit(err);
});

h.extend.generator.register('post', function(locals){
    console.log(locals);
    return locals.posts.map(function(post){
        return {
            path: post.path,
            data: post,
            layout: 'post'
        };
    });


});
router.get('/post/:id',function(req,res){
    var id = req.params.id;
    h.render.render({path: __dirname +'/../source/_posts/'+id+'.md'}).then(function(result){
       res.status(200).send(result);
    }).error(function(err){
        console.log(err);
        res.status(404).send(false);
    });

});
router.post('/createPost',function(req,res){
    var title = req.body.title;
    var content = req.body.content;
    console.log(content);
    var tags = req.body.tags;
    var articleId ='ART0'+Date.now();
    var data = {
        'title':title,
        'articleId': articleId,
        'tags':tags,
        'content': content

    };
    h.post.create(data).error(function(err){console.log(err)});
    res.redirect('post/'+articleId);

});
module.exports = router;