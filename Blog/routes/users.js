var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
/* GET users listing. */
router.get('/', function(req, res, next) {
  mongoose.model('users').find(function(err,users){
    if(err){
      console.log(err);
      res.send('error');
    }else {
    //  console.log(users);
      res.status(200).send(users);
    }
  });
//  res.send('respond with a resource');
});

module.exports = router;
