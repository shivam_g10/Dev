var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Ping Pong' });
});
router.post('/register',function(req,res){
  var onRegister = function(err,num){
    var name = req.body.name;
    var doc = {
      '_id' : num,
      'name' : name
    };
    var onInsert = function(err,docs){
      if(err){
        console.log(err.message);
        res.render('index', {response: "Name Already Exists"});

      }
      else{
          var name = docs[0]['name'];
          res.cookie('name',name,{maxAge: 86400000, signed: true });
          res.redirect('/home');
      }
    };
    Users.insert(doc,onInsert);
  };
    Users.register(onRegister);
});
module.exports = router;
