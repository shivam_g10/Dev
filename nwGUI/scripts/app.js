let API_KEY="";
let API_URL="";
let fs = require("fs");
let dataStorePath = "data/library.json"; 
fs.readFile("credentials/credentials.json","utf-8",(err,contents)=>{
    if(err){
        console.log(err);
    }else{
        try{
            contents = JSON.parse(contents);
            API_KEY = contents.API_KEY;
            API_URL = contents.API_URL;
        }catch(err){
            console.log(err);
        }
    }
});
let playlist = [];
fs.readFile(dataStorePath,"utf-8",(err,contents)=>{
    if(err){
        console.log(err);
    }else{
        try{
            playlist = JSON.parse(contents);
            resetPlaylist();
        }catch(err){
            console.log(err);
        }
    }
});
function changeVideo(index){
    let currentData = playlist[index];
    let parent = document.getElementById("content");
    parent.getElementsByClassName("card-title")[0].text = currentData.title;
    parent.getElementsByClassName("card-description")[0].text = currentData.description;
    parent.getElementsByClassName("title")[0].text = currentData.channelTitle;
    changeYTVideo(currentData.id);
}
function resetPlaylist(){
    let liTemp = document.getElementsByClassName("videoListItem")[0].cloneNode(true);
    let parent = document.getElementsByClassName("videoList")[0];
    document.getElementsByClassName("videoList")[0].innerHTML = "";
    for(video in playlist){
        let currentVideo = liTemp.cloneNode(true);
        currentVideo.getElementsByTagName("img")[0].src = playlist[video].thumbnails.default.url;
        currentVideo.getElementsByClassName("title")[0].text = playlist[video].title;
        currentVideo.getElementByTagName("a")[0].onclick = "changeVideo("+video+")";
        parent.appendChild(currentVideo);
    }
}
function fetchVideoDetails(videoId,callback){
    let xhr = new XMLHttpRequest();
    let url = API_URL.replace("APIKEY",API_KEY).replace("VIDEOID",videoId);
    xhr.onreadystatechange = ()=>{
        if(this.readyState == 4 ){
            if(this.status==200){
                
            }else{
                console.log(this.status,this.responseText)
            }
        }
    }
    xhr.open("GET",url,true);
    xhr.send();
}