let win = nw.Window.get();
win.isMaximized = false;
document.getElementById("windowClose").onclick = (e)=>{
    win.close();
};
document.getElementById("windowMinimize").onclick = (e)=>{
    win.minimize();
};
document.getElementById("windowMaximize").onclick = (e)=>{
    if (win.isMaximized)
        win.unmaximize();
    else{
        win.maximize();//$("header").show();$("body").css("padding-top","30px");
    }
};

win.on('maximize', function(){
    win.isMaximized = true;
});

win.on('restore', function(){
    win.isMaximized = false;
});
win.maximize();
var player;
function onYouTubeIframeAPIReady() {
  player = new YT.Player('player', {
    width: '100%',
    videoId: '49tpIMDy9BE',
    events: {
      'onStateChange': onPlayerStateChange
    }
  });
}
function changeYTVideo(videoId){
    player.loadVideoById(videoId);
}
// 4. The API will call this function when the video player is ready.
function onPlayerReady(event) {
  event.target.playVideo();
}

// 5. The API calls this function when the player's state changes.
//    The function indicates that when playing a video (state=1),
//    the player should play for six seconds and then stop.
var done = false;
function onPlayerStateChange(event) {
  if (event.data == YT.PlayerState.PLAYING && !done) {
   // setTimeout(stopVideo, 6000);
    done = true;
  }
}
function stopVideo() {
  player.stopVideo();
}
win.on('new-win-policy', function(frame, url, policy){
    policy.ignore();
  });
$("#playlist input[type=text]").focus();
win.on("resize",(e)=>{
    if(!win.isMaximized){
      //  $("header").fadeOut();
       // $("body").css("padding-top","0px");
    }
});
let menu = new nw.Menu();
menu.append(new nw.MenuItem({label:"Copy",click:()=>{ document.execCommand("copy");}}))
document.body.addEventListener('contextmenu', function(ev) {
    ev.preventDefault();
    menu.popup(ev.x, ev.y);
    return false;
});
